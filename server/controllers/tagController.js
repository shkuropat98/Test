/* eslint-disable no-empty-function */
/* eslint-disable class-methods-use-this */
const { Tag } = require('../models/models');
const ApiError = require('../error/ApiError');

class TagController {
  async create(req, res) {
    const { title } = req.body;
    const tag = await Tag.create({ title });
    return res.json(tag);
  }

  async getAll(req, res) {
    const tags = await Tag.findAll();
    return res.json({ tags });
  }

  async getOne(req, res) {

  }

  async update(req, res) {

  }

  async delete(req, res) {

  }
}
module.exports = new TagController();
