/* eslint-disable consistent-return */
/* eslint-disable class-methods-use-this */

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const ApiError = require('../error/ApiError');
const {
  User, Collection, Tag, Card,
} = require('../models/models');

const generateJwt = (id, email, role) => jwt.sign({ id, email, role }, process.env.SECRET_KEY, { expiresIn: '24h' });
class UserController {
  async registration(req, res, next) {
    const { email, password, role } = req.body;
    if (!email || !password) {
      return next(ApiError.badRequest('Нет логина или пароля'));
    }
    const candidate = await User.findOne({ where: { email } });
    if (candidate) {
      return next(ApiError.badRequest('Логин занят'));
    }
    const hashPassword = await bcrypt.hash(password, 2);
    const user = await User.create({ email, password: hashPassword, role });
    const firstUserCollection = await Collection.create({ title: 'Первая коллекция', userId: user.id });
    const token = generateJwt(user.id, user.email, user.role);
    return res.json({ token });
  }

  async login(req, res, next) {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });
    if (!user) {
      return next(ApiError.forbidden('Пользователь с таким логином не найден'));
    }
    const comparePassword = bcrypt.compareSync(password, user.password);
    if (!comparePassword) {
      return next(ApiError.forbidden('Указан неверный пароль'));
    }
    const token = generateJwt(user.id, user.email, user.role);
    return res.json({ token });
  }

  async check(req, res, next) {
    const token = generateJwt(req.user.id, req.user.email, req.user.role);
    return res.json({ token });
  }

  async getUserData(req, res) {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      const { id: userId } = jwt.verify(token, process.env.SECRET_KEY);
      const collectionsData = await Collection.findAll({
        where: { userId },
        include: [{
          model: Card,
        }],
      });
      const tagsData = await Tag.findAll({
        where: { userId },
        include: [{
          model: Card,
        }],
      });
      const userInfo = { collections: [...collectionsData], tags: [...tagsData] };
      return res.json({ userInfo });
    }
    return res.status(401).json({ message: 'Пользователь не авторизован' });
  }
}
module.exports = new UserController();
