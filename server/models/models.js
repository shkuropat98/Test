const sequelize = require('../db');
// eslint-disable-next-line import/order
const { DataTypes } = require('sequelize');

const User = sequelize.define('user', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  email: { type: DataTypes.STRING, unique: true },
  password: { type: DataTypes.STRING, unique: false },
  role: { type: DataTypes.STRING, defaultValue: 'USER' },
});

const Collection = sequelize.define('collection', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  title: { type: DataTypes.STRING, allowNull: false },
});

const Tag = sequelize.define('tag', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  title: { type: DataTypes.STRING, unique: true, allowNull: false },
});

const Card = sequelize.define('card', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  header: { type: DataTypes.STRING },
  img: { type: DataTypes.STRING },
  content: { type: DataTypes.STRING },
  url: { type: DataTypes.STRING, allowNull: false },
});

const TagCard = sequelize.define('tag_card', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
});

User.hasMany(Collection);
Collection.belongsTo(User);

User.hasMany(Tag);
Tag.belongsTo(User);

Collection.hasMany(Card);
Card.belongsTo(Collection);

Tag.belongsToMany(Card, { through: TagCard });
Card.belongsToMany(Tag, { through: TagCard });

module.exports = {
  User,
  Collection,
  Card,
  Tag,
  TagCard,
};
