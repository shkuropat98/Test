const Router = require('express');
const cardController = require('../controllers/cardController');

const router = new Router();

router.post('/', cardController.create);
router.get('/', cardController.getAll);
router.get('/:id', cardController.getOne);
router.put('/', cardController.update);
router.delete('/:id', cardController.delete);

module.exports = router;
