const Router = require('express');
const CollectionContriller = require('../controllers/collectionController');

const router = new Router();

router.post('/', CollectionContriller.create);
router.get('/', CollectionContriller.getAll);
router.get('/withCards', CollectionContriller.getAllWithCards);
router.get('/:id', CollectionContriller.getOne);
router.put('/', CollectionContriller.update);
router.delete('/:id', CollectionContriller.delete);

module.exports = router;
