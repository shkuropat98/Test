/* eslint-disable import/no-cycle */
import React, {
  useContext,
  useEffect,
  useState,
} from 'react';
import { BrowserRouter } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { MyContext } from './index';
import AppRouter from './components/AppRouter';

import { check } from './http/userAPI';

const App = observer(() => {
  const { user } = useContext(MyContext);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    check().then((data) => {
      user.setUser({ email: data.email });
      user.setIsAuth(true);
    }).finally(() => setLoading(false));
  }, []);
  if (loading) {
    return (<div>Loading...</div>);
  }
  return (
    <BrowserRouter>
      <AppRouter />
    </BrowserRouter>
  );
});
export default App;
