/* eslint-disable import/no-cycle */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useContext } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {
  Formik,
  Form,
} from 'formik';
import { createNewCollection } from '../../http/collectionAPI';
import { MyContext } from '../../index';
import styles from './AddCollectionForm.module.css';
import { addCollectionSchema } from '../../utils/validations/validations';
import { addCollectionInitialValues } from '../../utils/validations/const';

const AddCollectionForm = (props) => {
  const { tabs } = useContext(MyContext);

  return (
    <Formik
      initialValues={addCollectionInitialValues}
      validationSchema={addCollectionSchema}
      validateOnBlur={false}
      validateOnChange={false}
      onSubmit={async (values, actions) => {
        const newCollection = await createNewCollection(values.collectionTitle);
        tabs.createNewCollection(newCollection);
        actions.resetForm();
      }}
    >
      {
  ({
    values,
    handleChange,
    errors,
    touched,
  }) => (
    <Form className={styles.container}>
      <div className={styles.input}>
        <TextField
          fullWidth
          placeholder="collection"
          value={values.collectionTitle}
          id="collectionTitle"
          name="collectionTitle"
          onChange={handleChange}
          autoComplete="off"
          error={touched.collectionTitle && Boolean(errors.collectionTitle)}
          helperText={touched.collectionTitle && errors.collectionTitle}
        />
      </div>
      <div className={styles.button}>
        <Button type="submit" variant="contained" color="primary">
          +
        </Button>
      </div>
    </Form>
  )
}
    </Formik>
  );
};
export default AddCollectionForm;
