/* eslint-disable max-len */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import styles from './MainPage.module.css';
import CollectionsList from '../CollectionsList/CollectionsList';
import UserInfo from '../UserInfo/UserInfo';
import ContentItem from '../ContentItemLink/ContentItemLink';
import AddCollectionForm from '../AddCollectionForm/AddCollectionForm';
// import AddContentItemButton from '../AddContentItemButton/AddContentItemButton';
import Modal from '../Modal/Modal';
import ShowEditModalButton from '../ShowEditModalButton/ShowEditModalButton';

const MainPage = observer((props) => {
  const { tabs } = props; // объект
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [appState, setAppState] = useState(tabs);
  const [showModalEdit, setShowModalEdit] = useState(false);
  const [url, setUrl] = useState('url');

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        {showModalEdit
        && (
          <div className={styles.modalContainer}>
            <Modal
              showModalEdit={showModalEdit}
              setShowModalEdit={setShowModalEdit}
              linkProp={url}
              currentState={appState}
              setAppState={setAppState}
              activeTabIndex={activeTabIndex}
            />
          </div>
        )}
        <div className={styles.sideWrapper}>
          <UserInfo />
          <AddCollectionForm setAppState={setAppState} />
          <CollectionsList
            tabs={appState}
            activeTabIndex={activeTabIndex}
            setActiveTabIndex={setActiveTabIndex}
          />
        </div>
        <div className={styles.contentContainer}>
          {/* <AddContentItemButton
            currentState={appState}
            setAppState={setAppState}
            activeTabIndex={activeTabIndex}
          /> */}
          <ShowEditModalButton
            url={url}
            setUrl={setUrl}
            showModalEdit={showModalEdit}
            setShowModalEdit={setShowModalEdit}
          />
          <div className={styles.contentHeader}>
            {appState[activeTabIndex].label}
          </div>
          <div className={styles.contentBody}>
            {
              appState[activeTabIndex].content.map((contentObject) => <ContentItem contentObject={contentObject} />)
            }
          </div>
        </div>
      </div>
    </div>
  );
});
export default MainPage;
