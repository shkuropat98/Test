import React from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import styles from './UserInfo.module.css';

export default function userInfo({ username }) {
  return (
    <div className={styles.userInfoContainer}>
      <div className={styles.userPhoto}>
        <AccountCircleIcon style={{ fontSize: 25 }} />
      </div>
      <div className={styles.username}>
        {username}
      </div>
    </div>
  );
}
