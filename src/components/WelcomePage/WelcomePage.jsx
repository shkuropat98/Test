/* eslint-disable import/no-cycle */
import React, { useContext } from 'react';

import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import styles from './WelcomePage.module.css';
import { MyContext } from '../../index';

const WelcomePage = observer(() => {
  const { user } = useContext(MyContext);

  const history = useHistory();
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        { user.isAuth ? (
          <Button onClick={() => history.push('/main')} variant="contained" color="primary">К приложению</Button>
        )
          : (
            <>
              <Button onClick={() => history.push('/registration')} style={{ marginRight: '20px' }} variant="contained" color="primary">Регистрция</Button>
              <Button onClick={() => history.push('/auth')} variant="contained" color="primary">Войти</Button>
            </>
          )}
      </div>
      <div className={styles.content}>
        <div className={styles.title}>Менеджер закладок</div>
        <div className={styles.contentBody}>
          Данное приложение представляет собой единое хранилище браузерных закладок.
        </div>
      </div>
    </div>
  );
});

export default WelcomePage;
