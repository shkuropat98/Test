/* eslint-disable import/no-cycle */
/* eslint-disable import/prefer-default-export */
import React, { createContext } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserStore from './store/UserStore';
import TabStore from './store/TabStore';

const MyContext = createContext(null);
export { MyContext };

ReactDOM.render(
  <MyContext.Provider value={{
    user: new UserStore(),
    tabs: new TabStore(),
  }}
  >
    <App />
  </MyContext.Provider>,
  document.getElementById('root'),
);
