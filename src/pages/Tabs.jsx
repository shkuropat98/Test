/* eslint-disable max-len */
/* eslint-disable no-shadow */
/* eslint-disable import/no-cycle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/tabindex-no-positive */
import React, { useContext, useState } from 'react';
import './Tabs.css';
import { observer } from 'mobx-react-lite';
import { AiFillDelete } from 'react-icons/ai';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {
  Formik,
  Form,
} from 'formik';
import ContentItemLink from '../components/ContentItemLink/ContentItemLink';
import { MyContext } from '../index';
import { editCollection, deleteCollection } from '../http/collectionAPI';
import { editCollectionSchema } from '../utils/validations/validations';

export const TabItem = ({
  tabTitle, tabIndex, toggleState, setToggleState,
}) => {
  const { tabs } = useContext(MyContext);
  const [editCollectionModal, setEditCollectionModal] = useState(false);
  const editCollectionInitialValues = {
    collectionTitle: tabs.activeCollectionTab.title,
  };

  const deleteColl = async (collectionId, tabs) => {
    await deleteCollection(collectionId);
    tabs.deleteCollection(collectionId);
  };

  const editCollectionMode = () => {
    if (editCollectionModal) {
      setEditCollectionModal(false);
    } else {
      setEditCollectionModal(true);
    }
  };

  return (
    <div
      className={toggleState === tabIndex ? 'tabs active-tabs' : 'tabs'}
      onClick={() => {
        setToggleState(tabIndex);
        tabs.setActiveCollectionTab({
          id: tabIndex,
          title: tabTitle,
        });
        if (toggleState !== tabIndex) { setEditCollectionModal(false); }
      }}
    >
      <div
        onDoubleClick={() => { editCollectionMode(); }}
        style={{
          display: 'flex',
          userSelect: 'none',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        {(toggleState === tabIndex && editCollectionModal) ? (
          <Formik
            initialValues={editCollectionInitialValues}
            validationSchema={editCollectionSchema}
            validateOnBlur={false}
            validateOnChange={false}
            onSubmit={async (values) => {
              try {
                const newCollection = { id: tabs.activeCollectionTab.id, title: values.collectionTitle };
                await editCollection(newCollection);
                tabs.setActiveCollectionTab(newCollection);
                tabs.editActiveCollection(values.collectionTitle);
                setEditCollectionModal(false);
                editCollectionMode();
              } catch (error) {
                console.log(error);
              }
            }}
          >
            {
                ({
                  values,
                  handleChange,
                  errors,
                  touched,
                }) => (
                  <Form className="editCollectionFormContainer">
                    <TextField
                      value={values.collectionTitle}
                      id="collectionTitle"
                      name="collectionTitle"
                      onChange={handleChange}
                      autoComplete="off"
                      error={touched.collectionTitle && Boolean(errors.collectionTitle)}
                      helperText={touched.collectionTitle && errors.collectionTitle}
                    />
                    <Button style={{ marginLeft: '10px' }} size="small" type="submit" variant="outlined" color="primary">
                      ОК
                    </Button>
                  </Form>
                )
              }
          </Formik>

        ) : tabTitle}
        {
        toggleState === tabIndex
          ? (
            <div style={{ fontSize: '24px' }}>
              <AiFillDelete onClick={() => deleteColl(tabs.activeCollectionTab.id, tabs)} />
            </div>
          ) : null
        }
      </div>
    </div>
  );
};

export const TabContent = ({
  tabIndex, toggleState, cards,
}) => (
  <div className={toggleState === tabIndex ? 'content  active-content' : 'content'}>
    {
      cards ? (
        cards.map((card) => (
          <ContentItemLink
            card={card}
          />
        ))) : null
    }
  </div>
);
const Tabs = observer(({ tabs }) => {
  const [toggleState, setToggleState] = useState(tabs.collectionsWithCards[0].id);
  const [activeCard, setActiveCard] = useState(0);
  return (
    <div className="container">
      <div className="bloc-tabs">
        {
            tabs.collectionsWithCards.map((collection) => (
              <TabItem
                tabTitle={collection.title}
                tabIndex={collection.id}
                cards={collection.cards}
                toggleState={toggleState}
                setToggleState={setToggleState}
                tabs={tabs}
              />
            ))
        }
      </div>
      <div className="content-tabs">
        {
            tabs.collectionsWithCards.map((collection) => (
              <TabContent
                tabIndex={collection.id}
                cards={collection.cards}
                toggleState={toggleState}
                activeCard={activeCard}
                setActiveCard={setActiveCard}
              />
            ))
        }
      </div>
    </div>
  );
});

export default Tabs;
