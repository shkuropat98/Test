/* eslint-disable import/no-cycle */
import React from 'react';
import WelcomePage from '../components/WelcomePage/WelcomePage';

const Welcome = () => (<WelcomePage />);
export default Welcome;
