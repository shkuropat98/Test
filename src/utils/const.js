const MAIN_ROUTE = '/main';
const AUTH_ROUTE = '/auth';
const REGISTRATION_ROUTE = '/registration';
const WELCOME_ROUTE = '/welcome';

export {
  MAIN_ROUTE,
  AUTH_ROUTE,
  REGISTRATION_ROUTE,
  WELCOME_ROUTE,
};
