export const authInitialValues = {
  login: '',
  password: '',
};
export const addCollectionInitialValues = {
  collectionTitle: '',
};
export const addNewCardInitialValues = {
  cardUrl: '',
};
export const editCollectionInitialValues = {
  collectionTitle: '',
};
